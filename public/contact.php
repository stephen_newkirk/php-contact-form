<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
set_include_path($_SERVER['DOCUMENT_ROOT']);

require_once './app/controllers/MessageController.php';
require 'config.php';

use App\Controller\MessageController;

try {
  $connection = new PDO("mysql:host=$host", $username, $password, $options);

  $messageController = new MessageController($connection);

  $messageController->handleRequest($_POST);
} catch(\Exception $e) {
  echo $e->getMessage();
}

