$(function () {
  var form = $('#contact-form');
  var messageSpace = $('#contact-form-message');

  form.submit(function (event) {
    event.preventDefault();
    var formData = form.serialize();

    $.ajax({
      type: 'POST',
      url: window.location.href + 'contact.php',
      data: formData,
    }).done(function (response) {
      messageSpace.removeClass('alert-error');
      messageSpace.addClass('alert-success');
      messageSpace.text('Success! Thanks for contacting us.');

      $('#contact-fullname').val('');
      $('#contact-email').val('');
      $('#contact-phone').val('');
      $('#contact-message').val('');
    }).fail(function (data) {
      messageSpace.removeClass('alert-success');
      messageSpace.addClass('alert-danger');
      messageSpace.text(data.responseText || 'Error. The message was not sent.');
    });
  })
});