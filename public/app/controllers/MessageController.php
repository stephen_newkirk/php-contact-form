<?php

namespace App\Controller;

require_once './app/services/MessageService.php';
require_once './app/services/MailService.php';

use App\Service\MessageService;
use App\Service\MailService;

class MessageController
{
  private $messageService;
  private $mailService;
  
  function __construct($connection)
  {
    $this->messageService = new MessageService($connection);
    $this->mailService = new MailService();
  }

  public function handleRequest($postData)
  {
    try {
      $message = $this->messageService->saveMessage($postData);
      $this->mailService->sendEmail($message);
      echo \http_response_code(201);
    } catch (Exception $e) {
      echo 'Error: ' . $e->getCode();
    }
  }
}
