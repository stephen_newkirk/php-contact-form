<?php

namespace App\Test;

use PHPunit\Framework\TestCase;
use App\Service\MessageService;
use App\Model\Message;

class MessageServiceTest extends TestCase
{
  protected $messageService;
  
  protected $data;

  protected function setUp(): void
  {
    $pdoStatementStub = $this->createMock(\PDOStatement::class);
    $connectionStub = $this->createMock(\PDO::class);
    $connectionStub->method('prepare')
      ->willReturn($pdoStatementStub);
    $this->connectionStub = $connectionStub;

    $this->messageService = new MessageService($connectionStub);
    $this->data = [
      'fullname' => 'Full Name',
      'email' => 'test@test.com',
      'phone' => '123-456-7890',
      'message' => 'Hello there.',
    ];
  }

  public function testCreateMessage()
  {
    $result = $this->messageService->createMessage($this->data);

    $this->assertInstanceOf(Message::class, $result);
  }

  public function testSaveMessage()
  {
    $result = $this->messageService->saveMessage($this->data);

    $this->assertInstanceOf(Message::class, $result);
  }
}