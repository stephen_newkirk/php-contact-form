<?php

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Service\MailService;
use App\Model\Message;

class MailServiceTest extends TestCase
{
  protected $mailService;
  protected $message;

  protected function setUp(): void
  {
    $mailService = new MailService();
    
    $message = new Message();
    $message->setFullName('Full Name');
    $message->setEmail('test@test.com');
    $message->setPhone('616-123-4567');
    $message->setMessage('This is a message.');

    $this->mailService = $mailService;
    $this->message = $message;
  }

  public function testGenerateHtml()
  {
    $result = $this->mailService->generateHtml($this->message);
    
    $expected = "<html><body><h1>New Message</h1><p>Name: Full Name</p>" . 
    "<p>Email: test@test.com</p><p>Phone: 616-123-4567</p><p>Message: " . 
    "This is a message.</p></body></html>";

    $this->assertEquals($expected, $result);
  }

  public function testSendEmail()
  {
    $result = $this->mailService->sendEmail($this->message);

    $this->assertTrue($result);
  }
}
