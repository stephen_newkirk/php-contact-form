<?php

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Message as Message;

class MessageTest extends TestCase
{
  protected $message;

  protected function setUp(): void
  {
    $this->message = new Message();
  }

  public function testSetId()
  {
    $this->message->setId(1);

    $this->assertEquals(1, $this->message->getId());
  }

  public function testSetFullName()
  {
    $this->message->setFullName('Full Name');

    $this->assertEquals('Full Name', $this->message->getFullName());
  }

  public function testSetEmail()
  {
  $this->message->setEmail('test@test.com');

  $this->assertEquals('test@test.com', $this->message->getEmail());
  }

  public function testEmailValidation()
  {
    $this->expectException(\Exception::class);
    $this->message->setEmail('Not a valid email');
  }

  public function testSetPhone()
  {
    $this->message->setPhone('616-123-4567');

    $this->assertEquals('616-123-4567', $this->message->getPhone());
  }

    public function testPhoneValidation()
  {
    $this->expectException(\Exception::class);
    $this->message->setPhone('Not a valid phone number');
  }

  public function testSetMessage()
  {
    $this->message->setMessage('This is a message.');

    $this->assertEquals('This is a message.', $this->message->getMessage());
  }
}
