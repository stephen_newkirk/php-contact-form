<?php

namespace App\Service;

class MailService
{
  public function sendEmail($message)
  {
    $html = $this->generateHtml($message);
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    return mail('guy-smiley@example.com', 'New Message', $emailMessage, $headers);
  }

  public function generateHtml($message)
  {
    $html = '<html><body>';
    $html .= '<h1>New Message</h1>';
    $html .= '<p>Name: ' . $message->getFullName() . '</p>';
    $html .= '<p>Email: ' . $message->getEmail() . '</p>';
    $html .= $message->getPhone() ? '<p>Phone: ' . $message->getPhone() . '</p>' : '';
    $html .= '<p>Message: ' . $message->getMessage() . '</p>';
    $html .= '</body></html>';

    return $html;
  }
}
