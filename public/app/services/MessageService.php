<?php

namespace App\Service;

require_once __DIR__ . '/../models/Message.php';

use App\Model\Message;

class MessageService
{
  private $connection;

  function __construct($connection)
  {
    $this->connection = $connection;
  }

  public function saveMessage($data)
  {
    $message = $this->createMessage($data);

    $sql = 'INSERT INTO contactdb.message (fullname, email, phone, message) VALUES (:fullname, :email, :phone, :message)';

    $prepared = $this->connection->prepare($sql);

    try {
      $prepared->execute([
        ':fullname' => $message->getFullName(),
        ':email' => $message->getEmail(),
        ':phone' => $message->getPhone(),
        ':message' => $message->getMessage(),
      ]);
    } catch(\Exception $e) {
      throw new \Exception('Error saving message to db.');
    }

    return $message;
  }

  public function createMessage($data)
  {
    try {
      $message = new Message();
      $message->setFullName($data['fullname']);
      $message->setEmail($data['email']);
      $message->setPhone($data['phone']);
      $message->setMessage($data['message']);
    } catch (\Exception $e) {
      throw new \Exception('Error creating new message: ' . $e->getMessage());
    }

    return $message;
  }
}
