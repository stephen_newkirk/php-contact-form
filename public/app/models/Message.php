<?php

namespace App\Model;

class Message
{

  private $id;
  private $fullName;
  private $email;
  private $phone;
  private $message;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getFullName()
  {
    return $this->fullName;
  }

  public function setFullName($fullName)
  {
    $fullName = filter_var($fullName, FILTER_SANITIZE_STRING);

    $this->fullName = $fullName;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      throw new \Exception('Invalid email.');
    }

    $this->email = $email;
  }

  public function getPhone() {
    return $this->phone;
  }

  public function setPhone($phone)
  {
    $phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

    if(!preg_match('^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$^', $phone)) {
      throw new \Exception('Invalid phone number.');
    }

    $this->phone = $phone;
  }

  public function getMessage()
  {
    return $this->message;
  }

  public function setMessage($message)
  {
    $message = filter_var($message, FILTER_SANITIZE_STRING);
    
    $this->message = $message;
  }
}
