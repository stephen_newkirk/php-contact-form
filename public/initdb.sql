DROP DATABASE IF EXISTS contactdb;
DROP USER IF EXISTS 'user'@'127.0.0.1';

CREATE DATABASE contactdb;

use contactdb;

CREATE USER 'user' @'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL ON contactdb.* TO 'user'@'127.0.0.1';

CREATE TABLE message (
  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  fullname VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  phone VARCHAR(255),
  message VARCHAR(255) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);