<?php

$host       = "127.0.0.1";
$username   = "user";
$password   = "password";
$dbname     = "contactdb";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );