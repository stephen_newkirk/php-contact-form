# Dealer Inspire PHP Code Challenge - Stephen Newkirk

## Setup Steps
```
cd php-contact-form
mysql -u root -p < public/initdb.sql
composer install
vendor/bin/phpunit public/app/tests
php -S 127.0.0.1:9999 -t public
```